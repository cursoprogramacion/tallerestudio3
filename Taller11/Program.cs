﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller11
{
    class Program
    {
        static void Main(string[] args)
        {

            string strTemp;
            bool binTemp;
            byte opc;
            int n;
            Fibonacci fibSeq;
            Primos primos;

            do
            {
                Console.WriteLine("1: Fibonacci\n2: Numeros primos\n3: Salir");
                strTemp = Console.ReadLine();
                binTemp = byte.TryParse(strTemp, out opc);
            } while (!binTemp);

            // Menuu del programa
            switch (opc)
            {
                // Fibonnaci
                case 1:
                    do
                    {
                        Console.WriteLine("\nCalcular susecion de fibonacci hasta: ");
                        strTemp = Console.ReadLine();
                        binTemp = int.TryParse(strTemp, out n);
                    } while (!binTemp);
                    fibSeq = new Fibonacci(n);
                    // EL argumento del constructor limita el maximo numero de elementos de la sucecion a calcular
                    // por tanto este argumento limita a todos los demas metodos, este posible error se conoce, pero no se ha tratado
                    fibSeq.GetReverseSecuence();
                    Console.WriteLine("El {0}-avo termino de la sucesion de fibonacci es: {1}", n, fibSeq.GetNthNumber(n));
                    Console.WriteLine("La suma de los primeros {0} terminos de la sucesion de fibonacci es {1}", n, fibSeq.SumarNPrimerosNumeros(n));
                    Console.WriteLine("La multiplicacion de los primeros {0} terminos de la sucesion de fibonacci es {1} ", n, fibSeq.MultiplicarNPrimerosNumeros(n));
                    Console.WriteLine("Una aproximacion al numero de oro es: {0}", fibSeq.GetBestGoldenRatio());
                    Console.ReadKey();
                    break;
                // Numeros primos
                case 2:
                    do
                    {
                        Console.WriteLine("\nInserte un numero para realizar el test de primalidad: ");
                        strTemp = Console.ReadLine();
                        binTemp = int.TryParse(strTemp, out n);
                    } while (!binTemp);
                    primos = new Primos(n); // Test de primalidad
                    Console.ReadKey();
                    break;

                // Salir
                case 3:
                    break;
                default:
                    break;
            }
            
        }
    }

    public class Fibonacci
    {
        private List<int> fibonacciSequence = new List<int>();

        public Fibonacci(int NthMaxTerm)
        {
            fibonacciSequence.Add(0);
            fibonacciSequence.Add(1);

            int i = 2;
            while (i <= NthMaxTerm)
            {
                fibonacciSequence.Add(fibonacciSequence[i - 1] + fibonacciSequence[i - 2]);
                i++;
            }
        }

        // Funcion para hallar el n-simo numero en la secuensia de fibonacci
        /*
        termino 0 = 0
        termino 1 = 1
        termino 2 = 1
        termino 3 = 2
        termino 4 = 3
        termino 5 = 5
        termino 6 = 8
        termino 7 = 13
        ...
        */
        public int GetNthNumber(int n)
        {
            return fibonacciSequence[n];
        }

        public int SumarNPrimerosNumeros(int n)
        {
            int sum = 0;

            int i = 0;
            while (i <= n)
            {
                sum += fibonacciSequence[i];
                i++;
            }
            return sum;
        }

        public int MultiplicarNPrimerosNumeros(int n)
        {
            int multiplicativo = 1;
            int i = 1;
            while (i <= n)
            {
                multiplicativo *= fibonacciSequence[i];
                i++;
            }
            return multiplicativo;
        }

        public void GetReverseSecuence()
        {
            List<int> reverseSeq = fibonacciSequence;
            reverseSeq.Reverse();

            Console.Write("...");
            foreach (uint term in reverseSeq)
            {
                Console.Write(", " + term);
            }
            Console.WriteLine();
        }

        // Halla una aproximacion al radio de oro, usando la sucesion de fibonacci
        public float GetBestGoldenRatio()
        {
            return fibonacciSequence[fibonacciSequence.Count - 1] / fibonacciSequence[fibonacciSequence.Count - 2];
        }
    }

    public class Primos
    {
        public Primos(int n)
        {
            int contador = 1;

            if (n == 2 || n == 1)
            {
                Console.WriteLine("{0} es un numero primo", n);
            }
            else
            {            
                while (contador < n)
                {
                    contador++;
                    if (n % contador == 0  &&  contador != n)
                    {
                        Console.WriteLine("{0} no es un numero primo", n);
                        break;
                    }
                    else if (contador == n)
                    {                        
                        Console.WriteLine("{0} es un numero primo", n);
                        break;
                    }
                }
            }
            
        }
    }
}